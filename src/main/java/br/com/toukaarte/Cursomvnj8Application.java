package br.com.toukaarte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cursomvnj8Application {

	public static void main(String[] args) {
		SpringApplication.run(Cursomvnj8Application.class, args);
	}

}
